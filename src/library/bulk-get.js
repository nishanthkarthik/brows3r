const { ZipWriter, BlobWriter, HttpReader } = require('@zip.js/zip.js/dist/zip.min')
import s3Client from '@/store/s3'
import store from '@/store/index'
import { saveAs } from 'file-saver'

const state = store.state

async function listAllObjects(prefix) {
  const objects = []
  const listParams = {
    Bucket: state.selected.bucket,
    Prefix: prefix,
  };
  const begin = await s3Client.listObjectsV2(listParams).promise()
  Array.prototype.push.apply(objects, begin.Contents)
  let current = begin
  while (current.IsTruncated) {
    current = await s3Client.listObjectsV2({
      ...listParams,
      ContinuationToken: current.NextContinuationToken
    }).promise()
    Array.prototype.push.apply(objects, current.Contents)
  }
  return objects
}

export default async function bulkGetObjects(prefix, outputArchiveName) {
  const objects = await listAllObjects(prefix)
  const nameUrls = objects.map((e) => {
    return [e.Key, s3Client.getSignedUrl('getObject', {
      Bucket: state.selected.bucket,
      Key: e.Key,
    })]
  })
  const writer = new ZipWriter(new BlobWriter('application/zip'), {
    level: 3,
    bufferedWrite: true,
    keepOrder: false,
    useWebWorkers: true,
  })
  await Promise.all(nameUrls.map(([name, url]) => {
    const reader = new HttpReader(url, { preventHeadRequest: true })
    return writer.add(name, reader)
  }))
  const blob = await writer.close()
  saveAs(blob, outputArchiveName)
}