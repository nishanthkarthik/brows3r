import {createStore} from 'vuex'
import s3Client from '@/store/s3'

export default createStore({
  state: function () {
    return {
      message: null,
      config: {
        signedUrl: false,
      },
      buckets: [],
      selected: {
        bucket: null,
        chunk: null,
        items: null,
      }
    }
  },
  mutations: {
    setSignedConfig(state, value) {
      state.config.signedUrl = value
    },
    setBuckets(state, value) {
      this.commit('resetPath')
      state.buckets = value
    },
    resetPath(state) {
      state.selected.bucket = null
      state.selected.chunk = null
      state.selected.items = null
    },
    setPath(state, value) {
      state.selected.bucket = value.bucket
      state.selected.chunk = value.chunk
    },
    setItems(state, value) {
      state.selected.items = value
    },
    setSignedUrl(state) {
      state.selected.items?.Contents.forEach((item) => {
        item.signedUrl = item.signedUrl || s3Client.getSignedUrl('getObject', {
          Bucket: state.selected.bucket,
          Key: item.Key,
        })
      })
    },
    setMessage(state, value) {
      state.message = value
    },
  },
  actions: {
    async fetchBuckets(context) {
      const response = await s3Client.listBuckets().promise()
      context.commit('setBuckets', response.Buckets)
    },
    async fetchItems(context) {
      const Contents = []
      const CommonPrefixes = []
      const commit = () => {
        context.commit('setItems', { Contents, CommonPrefixes })
        context.commit('setSignedUrl')
      }

      const response = await s3Client.listObjectsV2({
        Bucket: context.state.selected.bucket,
        Prefix: context.state.selected.chunk,
        Delimiter: '/',
      }).promise()

      const push = Array.prototype.push
      push.apply(Contents, response.Contents)
      push.apply(CommonPrefixes, response.CommonPrefixes)

      let current = response

      while (current.IsTruncated) {
        current = await s3Client.listObjectsV2({
          Bucket: current.Name,
          Prefix: current.Prefix,
          ContinuationToken: current.NextContinuationToken,
          Delimiter: current.Delimiter,
        }).promise()
        push.apply(Contents, current.Contents)
        push.apply(CommonPrefixes, current.CommonPrefixes)
      }
      commit()
    }
  },
  modules: {}
})
