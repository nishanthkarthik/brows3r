import S3  from 'aws-sdk/clients/s3'

const s3Url = new URL(process.env.VUE_APP_SERVER_URL)

const s3Credentials = {
  accessKey: process.env.VUE_APP_SERVER_ACCESS_KEY,
  secretKey: process.env.VUE_APP_SERVER_SECRET_KEY,
}

const s3Client = new S3({
  endpoint: s3Url.toString(),
  credentials: {
    accessKeyId: s3Credentials.accessKey,
    secretAccessKey: s3Credentials.secretKey
  },
  s3ForcePathStyle: true,
  signatureVersion: 'v4',
})

export default s3Client
