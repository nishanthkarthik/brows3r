import {createRouter, createWebHistory} from 'vue-router'
import SelectBucket from '@/views/SelectBucket';
import Home from '@/views/Home';
import BrowseBucket from '@/views/BrowseBucket';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    children: [{
      path: '',
      component: SelectBucket,
    }, {
      path: ':bucket/:chunk(.*)?',
      component: BrowseBucket,
    }]
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
